import React from 'react'
import './App.css';
import ToDoItem from './toDo/ToDoItem/ToDoItem.js';
import todosData from './toDo/todosData.js';

const App = () => {
  const todosItem = todosData.map(item => {
    return (
      <ToDoItem
      key={item.id}
      description={item.text}
      completed={item.completed}
      />
    )
  });

  const greenTitle = {
    color: 'green',
  }

  const redTitle = {
    color: 'red'
  }

  return (
    <div className="App">
      <h1 style={todosItem ? greenTitle : redTitle}>My todoApp</h1>
      {todosItem}
    </div>
  );
}

export default App;
